#include "thread.hpp"

Thread::Thread() {

}

Thread::~Thread() {

}

bool Thread::create(void *(*startRoutine)(void *), void *params) {
  if (pthread_create(&this->thread, NULL, startRoutine, params)) {
    std::cerr << "Error creating thread" << std::endl;
    return false;
  }
  return true;
}

void Thread::exit(void *returnValue) {
  pthread_exit(returnValue);
}

bool Thread::join() {
  if (pthread_join(this->thread, NULL)) {
    std::cerr << "Error creating thread" << std::endl;
    return false;
  }
  return true;
}

bool Thread::cancel() {
  if (pthread_cancel(this->thread)) {
    std::cerr << "Error cancelling thread" << std::endl;
    return false;
  }
  return true;
}

bool Thread::detach() {
  if (pthread_detach(this->thread)) {
    std::cerr << "Error detaching thread" << std::endl;
    return false;
  }
  return true;
}

int Thread::self() {
  return pthread_self();
}

bool Thread::equal(Thread thread) {
  if (pthread_equal(this->thread, thread.getThread())) {
    return false;
  }
  return true;
}

pthread_t Thread::getThread() {
  return this->thread;
}
