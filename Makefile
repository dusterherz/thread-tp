CXX=g++
SOURCE=thread.cpp
NAME=tp3

all: $(NAME) library

$(NAME): $(SOURCE)

	$(CXX) -c $(SOURCE) -lpthread

library:

	ar rvs thread.a *.o

clean:

	rm -f $(NAME)
	rm -f *.o
