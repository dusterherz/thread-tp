#include <pthread.h>
#include <ostream>
#include <iostream>

class Thread {

  Thread();
  ~Thread();

  public:
    bool create(void *(*startRoutine)(void *), void *params);
    void exit(void *returnValue);
    bool join();
    bool cancel();
    bool detach();
    int self();
    bool equal(Thread thread);
    pthread_t getThread();

  private:
    pthread_t thread;
};
